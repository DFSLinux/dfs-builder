#!/bin/bash

# Set some colors
RED="\e[31m"
GREEN="\e[32m"
ENDCOLOR="\e[0m"

# echo -e "${RED}This is some red text, ${ENDCOLOR}"
# echo -e "${GREEN}And this is some green text${ENDCOLOR}"

# Check for some present, absent or missing libraries:
echo -e "\nThe (.la) and (.so) files identified by this script"
echo -e "\n should be all present or all absent,"
echo -e "\n but not only one or two present:"

echo ""
echo -e "\n###################################"
echo -e "\n### CHECKING THE (.la) FILES... ###"
echo -e "\n###################################"
sleep 1s
for lib in lib{gmp,mpfr,mpc,xml2,nettle,zstd}.la; do
  echo $lib: $(if find /usr/lib* -name $lib|
  grep -q $lib;then :;else echo -e "${RED}NOT${ENDCOLOR}";fi) found:  /usr/lib* was searched. 
done
unset lib

echo ""
echo -e "\n###################################"
echo -e "\n### CHECKING THE (.so) FILES... ###"
echo -e "\n###################################"
sleep 1s
for lib in lib{gmp,mpfr,mpc,xml2,nettle,zstd}.so*; do
  echo $lib: $(if find /usr/lib* -name $lib|
  grep -q $lib;then :;else echo -e "${RED}NOT${ENDCOLOR}";fi) found:  /usr/lib* was searched. 
done
unset lib


echo ""
echo -e "\n##################################"
echo -e "\n### CHECKING PYTHON VERSION... ###"
echo -e "\n##################################"
sleep 1s
python2 --version | head -n1

python3 --version | head -n1

echo ""
echo ""
